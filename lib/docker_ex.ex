defmodule DockerEx do
  @moduledoc """
  Documentation for `DockerEx`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> DockerEx.hello()
      :world

  """
  def hello do
    :world
  end
end
